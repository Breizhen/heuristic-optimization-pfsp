library(xtable)

# Exercise 1

val <- read.csv("./dat/Ex111_dev.csv")
bkv <- read.csv(file = "Material/Best-known Values", header = FALSE,  sep = " ", row.names = NULL,  stringsAsFactors = FALSE)$V2
time <- read.csv("./dat/Ex111_time.csv")

valMat <- data.matrix(val[c("Dev1","Dev2","Dev3","Dev4","Dev5")])
timeMat <- data.matrix(time[c("Time1","Time2","Time3","Time4","Time5")])

bestKnownVal <- rep(bkv, each=12)
#print(bestKnownVal)
devMat <- 100*(valMat-bestKnownVal)/bestKnownVal

#****First question observation***#

# meanTime <- rowMeans(timeMat)
# meanDev <- rowMeans(devMat)

# analyzeTime <- matrix(0,12,2)
# analyzeDev <- matrix(0,12,2)

# for (i in 1:12) { #iterate over algorithms
#     #time / size = 50
#     x <- mean(meanTime[seq(i, 120, 12)])
#     analyzeTime[i,1] <- x

#     #time / size = 100
#     x <- mean(meanTime[seq(120+i, 240, 12)])
#     analyzeTime[i,2] <- x

#     #deviation / size = 50
#     x <- mean(meanDev[seq(i, 120, 12)])
#     analyzeDev[i,1] <- x

#     #deviation / size = 100
#     x <- mean(meanDev[seq(120+i, 240, 12)])
#     analyzeDev[i,2] <- x
# }

#****Wilcoxon test****#

# 1. Initial Solution

# WilcoxTF <- wilcox.test(devMat[seq(1, 240, 12),], devMat[seq(7, 240, 12),], paired=T)$p.value
# print(WilcoxTF)

# WilcoxTB <- wilcox.test(devMat[seq(2, 240, 12),], devMat[seq(8, 240, 12),], paired=T)$p.value
# print(WilcoxTB)

# WilcoxEF <- wilcox.test(devMat[seq(3, 240, 12),], devMat[seq(9, 240, 12),], paired=T)$p.value
# print(WilcoxEF)

# WilcoxEB <- wilcox.test(devMat[seq(4, 240, 12),], devMat[seq(10, 240, 12),], paired=T)$p.value
# print(WilcoxEB)

# WilcoxIF <- wilcox.test(devMat[seq(5, 240, 12),], devMat[seq(11, 240, 12),], paired=T)$p.value
# print(WilcoxIF)

# WilcoxIB <- wilcox.test(devMat[seq(6, 240, 12),], devMat[seq(12, 240, 12),], paired=T)$p.value
# print(WilcoxIB)

# 2. Neighborhood
# #   a) Transpose / Exchange

# WilcoxRF <- wilcox.test(devMat[seq(1, 240, 12),], devMat[seq(3, 240, 12),], paired=T)$p.value
# print(WilcoxRF)

# WilcoxRB <- wilcox.test(devMat[seq(2, 240, 12),], devMat[seq(4, 240, 12),], paired=T)$p.value
# print(WilcoxRB)

# WilcoxSF <- wilcox.test(devMat[seq(7, 240, 12),], devMat[seq(9, 240, 12),], paired=T)$p.value
# print(WilcoxSF)

# WilcoxSB <- wilcox.test(devMat[seq(8, 240, 12),], devMat[seq(10, 240, 12),], paired=T)$p.value
# print(WilcoxSB)

# print("")

# #   b) Transpose / Insert

# WilcoxRF <- wilcox.test(devMat[seq(1, 240, 12),], devMat[seq(5, 240, 12),], paired=T)$p.value
# print(WilcoxRF)

# WilcoxRB <- wilcox.test(devMat[seq(2, 240, 12),], devMat[seq(6, 240, 12),], paired=T)$p.value
# print(WilcoxRB)

# WilcoxSF <- wilcox.test(devMat[seq(7, 240, 12),], devMat[seq(11, 240, 12),], paired=T)$p.value
# print(WilcoxSF)

# WilcoxSB <- wilcox.test(devMat[seq(8, 240, 12),], devMat[seq(12, 240, 12),], paired=T)$p.value
# print(WilcoxSB)

# print("")

# #   c) Exchange / Insert

# WilcoxRF <- wilcox.test(devMat[seq(3, 240, 12),], devMat[seq(5, 240, 12),], paired=T)$p.value
# print(WilcoxRF)

# WilcoxRB <- wilcox.test(devMat[seq(4, 240, 12),], devMat[seq(6, 240, 12),], paired=T)$p.value
# print(WilcoxRB)

# WilcoxSF <- wilcox.test(devMat[seq(9, 240, 12),], devMat[seq(11, 240, 12),], paired=T)$p.value
# print(WilcoxSF)

# WilcoxSB <- wilcox.test(devMat[seq(10, 240, 12),], devMat[seq(12, 240, 12),], paired=T)$p.value
# print(WilcoxSB)

# 3. Pivoting rule

# WilcoxRT <- wilcox.test(devMat[seq(1, 240, 12),], devMat[seq(2, 240, 12),], paired=T)$p.value
# print(WilcoxRT)

# WilcoxRE <- wilcox.test(devMat[seq(3, 240, 12),], devMat[seq(4, 240, 12),], paired=T)$p.value
# print(WilcoxRE)

# WilcoxRI <- wilcox.test(devMat[seq(5, 240, 12),], devMat[seq(6, 240, 12),], paired=T)$p.value
# print(WilcoxRI)

# WilcoxST <- wilcox.test(devMat[seq(7, 240, 12),], devMat[seq(8, 240, 12),], paired=T)$p.value
# print(WilcoxST)

# WilcoxSE <- wilcox.test(devMat[seq(9, 240, 12),], devMat[seq(10, 240, 12),], paired=T)$p.value
# print(WilcoxSE)

# WilcoxSI <- wilcox.test(devMat[seq(11, 240, 12),], devMat[seq(12, 240, 12),], paired=T)$p.value
# print(WilcoxSI)




# Exercise 2

val2 <- read.csv("./dat/Ex12_dev.csv")
time2 <- read.csv("./dat/Ex12_time.csv")

val2mat <- data.matrix(val2["Val"])
time2mat <- data.matrix(time2["Time"])

bestKnownVal <- rep(bkv, each=4)
dev2mat <- 100*(val2mat-bestKnownVal)/bestKnownVal

analyzeTime2 <- matrix(0,4,2)
analyzeDev2 <- matrix(0,4,2)


analyzeTime2[1,1] <- mean(time2mat[seq(1, 40, 4)])
analyzeTime2[2,1] <- mean(time2mat[seq(2, 40, 4)])
analyzeTime2[3,1] <- mean(time2mat[seq(3, 40, 4)])
analyzeTime2[4,1] <- mean(time2mat[seq(4, 40, 4)])
analyzeTime2[1,2] <- mean(time2mat[seq(41, 80, 4)])
analyzeTime2[2,2] <- mean(time2mat[seq(42, 80, 4)])
analyzeTime2[3,2] <- mean(time2mat[seq(43, 80, 4)])
analyzeTime2[4,2] <- mean(time2mat[seq(44, 80, 4)])

analyzeDev2[1,1] <- mean(dev2mat[seq(1, 40, 4)])
analyzeDev2[2,1] <- mean(dev2mat[seq(2, 40, 4)])
analyzeDev2[3,1] <- mean(dev2mat[seq(3, 40, 4)])
analyzeDev2[4,1] <- mean(dev2mat[seq(4, 40, 4)])
analyzeDev2[1,2] <- mean(dev2mat[seq(41, 80, 4)])
analyzeDev2[2,2] <- mean(dev2mat[seq(42, 80, 4)])
analyzeDev2[3,2] <- mean(dev2mat[seq(43, 80, 4)])
analyzeDev2[4,2] <- mean(dev2mat[seq(44, 80, 4)])

print(analyzeTime2)
print("")

print(analyzeDev2)
print("")

WilcoxR50 <- wilcox.test(dev2mat[seq(1, 40, 4)], dev2mat[seq(2, 40, 4)], paired=T)$p.value
print(WilcoxR50)

WilcoxS50 <- wilcox.test(dev2mat[seq(3, 40, 4)], dev2mat[seq(4, 40, 4)], paired=T)$p.value
print(WilcoxS50)

WilcoxR100 <- wilcox.test(dev2mat[seq(41, 80, 4)], dev2mat[seq(42, 80, 4)], paired=T)$p.value
print(WilcoxR100)

WilcoxS100 <- wilcox.test(dev2mat[seq(43, 80, 4)], dev2mat[seq(44, 80, 4)], paired=T)$p.value
print(WilcoxS100)







