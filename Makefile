program : src/pfspinstance.h src/flowshop.cpp src/pfspinstance.cpp

	g++ -O3 -c -std=c++20 ./src/flowshop.cpp -o src/flowshop.o
	g++ -O3 -c -std=c++20 ./src/pfspinstance.cpp -o src/pfspinstance.o

	g++ -O3 -std=c++20 src/flowshop.o src/pfspinstance.o -o flowshopWCT

clean:
	rm src/*.o flowshopCWT
