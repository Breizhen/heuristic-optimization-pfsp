# Read Me - Heuristic Optimization - PFSP 

## Contents

### **`src` folder**

Contains the C++ files (classes and functions definition) as defined in the report file (`Report.pdf`).

### **`instances` folder**

Contains all the instances used for the analyses of the programms.

### **`dat` folder**
Contains the CSV files with the weighted tardinnes and the computation time of the different algorithms on the different insatnces.

### **`analysis` folder**

Contains the R file made to analyze the data in the CSV files presented above.


***

## Execution

To compile and execute the main function of the program, the user should open a terminal in the `heuristic-optimization-pfsp` folder and use the following command:

```
make && ./flowshopWCT --ii <initSol> <neighborhood> <pivotRule> <pathToInstance>
```
for the classic itereative improvement algorithm, or
```
make && ./flowshopWCT --vnd <initSol> <nbhOrder> <pathToInstance>
```
for the VND algorithm, with: 

- `<initSol>` can take the following values: `--randomInit` or `--srz` (represents the initial solution used)
- `<neighborhood>` can take the following values: `--transpose`, `--exchange`, or `--insert` (represents the neighborhood used)
- `<pivotRule>` can take the following values: `--first` or `--best` (represents the pivot rule used) 
- `<nbhOrder>` can take the following values: `--tei` or `--tie` (represents the order of the neighborhood used).
- `<pathToInstance>` should be replaced with the relative path to the instance file chosen.

### Output

The function does not return anything and prints the Weighted Tardiness of the instance and the computation time for the algorithm indicated in parameters.